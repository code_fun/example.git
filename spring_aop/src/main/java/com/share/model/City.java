package com.share.model;

import lombok.Data;

import java.io.Serializable;

/**
*
* @date 2018/09/21
* @author caifan 
*
*/
@Data
public class City implements Serializable {
    private Integer id;
    private Integer provinceId;
    private String cityName;
    private String description;
}
