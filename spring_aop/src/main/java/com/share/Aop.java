package com.share;

import com.share.config.AnnotationConfig;
import com.share.dao.UserDao;
import com.share.dao.UserDaoImpl;
import com.share.mapper.CityMapper;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
*
* @date 2018/09/10
* @author caifan 
*
*/
public class Aop {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AnnotationConfig.class);
        UserDao userDao = ctx.getBean(UserDao.class);
        userDao.queryAll();
        System.out.println("===args===");
        CityMapper cityMapper = ctx.getBean(CityMapper.class);
        cityMapper.getById(1);

    }
}
