package com.share.dao;

import com.share.annotation.AopAnnotation;
import org.springframework.stereotype.Repository;

/**
*
* @date 2018/09/10
* @author caifan 
*
*/
public interface UserDao {
    public void queryAll();
}
