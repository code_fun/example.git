package com.share.dao;

import com.share.annotation.AopAnnotation;
import org.springframework.stereotype.Repository;

/**
*
* @date 2018/09/10
* @author caifan 
*
*/
@Repository
public class UserDaoImpl implements UserDao {

    @Override
    public void queryAll() {
        System.out.println("userDaoImpl queryAll");
    }
}
