package com.share.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
*
* @date 2018/09/10
* @author caifan 
*
*/
//@Aspect
@Component
public class MyAspect {

    /**
     * 设置切点位置
     */
    @Pointcut("execution(public * com.share.dao.*.*(..))")
    public void anyMethod() {
        System.out.println("all method");
    }

    @Pointcut("within(com.share.dao.*.*)")
    public void withMethod() {
        System.out.println("aop within ....");
    }

    /**
     * 前置通知
     */
    @Before("com.share.aspect.MyAspect.anyMethod()")
    public void before() {
        System.out.println("before...");
    }

    /**
     * 后置通知
     */
    @After("com.share.aspect.MyAspect.anyMethod()")
    public void after() {
        System.out.println("after ....");
    }

    @Around("com.share.aspect.MyAspect.anyMethod()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable{
        System.out.println("start...");
        Object proceed = joinPoint.proceed();
        System.out.println("end..");
        return proceed;
    }

    @Before("withMethod()")
    public void withinBefore() {
        System.out.println("within before ");
    }
}
