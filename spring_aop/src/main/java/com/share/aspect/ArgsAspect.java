package com.share.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
*
* @date 2018/09/22
* @author caifan 
*
*/
@Aspect
@Component
public class ArgsAspect {

    @Pointcut("args(Integer, ..))")
    public void args() {

    }

    @Before("com.share.aspect.ArgsAspect.args())")
    public void argsBefore() {
        System.out.println("args before...");
    }

    @Before("@args(com.share.annotation.MonitorAnnotation)")
    public void argsAnnotationBefore() {
        System.out.println("args MonitorAnnotation before...");
    }
}
