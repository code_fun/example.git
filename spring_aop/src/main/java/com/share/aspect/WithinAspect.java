package com.share.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
*
* @date 2018/09/10
* @author caifan 
*
*/
@Aspect
@Component
public class WithinAspect {

    /**
     * 设置切点位置
     * 如果表达式是within(com.share.dao.*.*则不会起作用，表名了within的作用范围是类级别
     */
    @Pointcut("within(com.share.dao.*)")
    public void withMethod() {
        System.out.println("aop within ....");
    }

    @Before("withMethod()")
    public void withinBefore() {
        System.out.println("within before ");
    }
}
