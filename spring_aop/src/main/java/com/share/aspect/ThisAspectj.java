package com.share.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
*
* @date 2018/09/22
* @author caifan 
*
*/
@Aspect
@Component
public class ThisAspectj {

    @Pointcut("this(com.share.dao.UserDaoImpl)")
    public void pointCutThis() {

    }

    @Pointcut("target(com.share.dao.UserDaoImpl)")
    public void pointCutTarget() {

    }

    /**
     * @EnableAspectJAutoProxy(proxyTargetClass = true)
     * 默认false false表示作用在代理类上，使用jdk动态代理 true表示作用目标类 使用的cglib代理 cglib代理的父类
     * @param joinPoint
     */
    @Before("pointCutThis()")
    public void beforeThis(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        Arrays.stream(args).forEach(System.out::print);
        System.out.println("this ....");
    }

    @Before("pointCutTarget()")
    public void beforeTarget(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        Arrays.stream(args).forEach(System.out::print);
        System.out.println("target ....");
    }

}
