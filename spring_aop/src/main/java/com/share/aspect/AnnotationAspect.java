package com.share.aspect;

import com.share.annotation.AopAnnotation;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
*
* @date 2018/09/22
* @author caifan 
*
*/
@Aspect
@Component
public class AnnotationAspect {

    @Pointcut("@annotation(com.share.annotation.MonitorAnnotation)")
    public void annotationMethod() {
        System.out.println("hhh");
    }

    @Before("annotationMethod()")
    public void annotation() {
        System.out.println("before annotation ");
    }
}
