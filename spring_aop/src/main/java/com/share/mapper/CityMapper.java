package com.share.mapper;

import com.share.annotation.MonitorAnnotation;
import com.share.model.City;
import org.apache.ibatis.annotations.*;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Repository;

/**
*
* @date 2018/09/21
* @author caifan 
*
*/
@Repository
//@Mapper

public interface CityMapper {
    @Results(id = "cityMap", value = {
            @Result(column="province_id", property="provinceId"),
            @Result(column="city_name", property="cityName")
    })
    @Select("select * from city where id = #{id}")
    @MonitorAnnotation
    City getById(Integer id);

    @Select("select city_name, description from city where id=#{id}")
    @ResultMap("cityMap")
    City getCityNameById(Integer id);
}
