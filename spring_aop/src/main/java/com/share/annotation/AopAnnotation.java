package com.share.annotation;

import java.lang.annotation.*;

/**
 * @author caifan
 * @date 2018/09/22
 */
//@Inherited
//@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AopAnnotation {
}
