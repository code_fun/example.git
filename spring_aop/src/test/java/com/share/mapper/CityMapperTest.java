package com.share.mapper;

import com.share.config.AnnotationConfig;
import com.share.config.DruidConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
*
* @date 2018/09/21
* @author caifan 
*
*/
@RunWith(SpringRunner.class)
@TestPropertySource(value = {"classpath:jdbc.properties"})
@ContextConfiguration(classes = {AnnotationConfig.class, DruidConfig.class})
public class CityMapperTest {
    @Autowired
    CityMapper cityMapper;
    @Test
    public void test() {
        System.out.println(cityMapper.getById(1));
    }

    @Test
    public void cityNameTest() {
        System.out.println(cityMapper.getCityNameById(1));
    }
}
