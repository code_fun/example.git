# example

#### 项目介绍
spring-aop示例代码

#### 说明
spring-aop中 execution within this target annotation arg的示例
1.execution与within的区别
execution可以作用在任何类 方法 上
within只能作用在类上
2.annotation表示支持定义的注解上其中注解的级别是方法级别
3.arg是作用在参数级别上
4.this和target区别
this作用在代理类，使用的jdk动态代理
target作用在目标类上，使用的cglib代理
